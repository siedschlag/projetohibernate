import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class main {



    public static void main(String args[]){

        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("org.hibernate.tutorial.jpa");
        Cliente cliente=new Cliente();
        cliente.setId(2);
        cliente.setName("Bob");


        Bank bank = new Bank();
        bank.setName("Bradesco");
        bank.setIdade("32");


        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        entityManager.merge(cliente);//persist quando esta criando
        entityManager.merge(bank);
        entityManager.getTransaction().commit();


        entityManagerFactory.close();



    }
}
